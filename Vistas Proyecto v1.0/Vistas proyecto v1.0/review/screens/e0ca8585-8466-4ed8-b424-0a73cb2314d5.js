var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1440" deviceHeight="900">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1024" height="768">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677117994478.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-e0ca8585-8466-4ed8-b424-0a73cb2314d5" class="screen growth-vertical devWeb canvas PORTRAIT firer commentable non-processed" alignment="left" name="Website login" width="1440" height="900">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/e0ca8585-8466-4ed8-b424-0a73cb2314d5-1677117994478.css" />\
      <div class="freeLayout">\
      <div id="s-Group_5" class="group firer ie-background commentable non-processed" customid="Fondo" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_3" class="rectangle manualfit firer commentable non-processed" customid="Base"   datasizewidth="1287.0px" datasizeheight="789.0px" datasizewidthpx="1287.0" datasizeheightpx="788.9999999999999" dataX="59.7" dataY="56.5" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_3_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Image_1" class="image lockH firer ie-background commentable non-processed" customid="Image 1"   datasizewidth="682.1px" datasizeheight="789.9px" dataX="695.7" dataY="56.5" aspectRatio="0.8635579"   alt="image">\
          <div class="borderLayer">\
          	<div class="imageViewport">\
          		<img src="./images/83f72f51-ccf2-4c3c-807c-b96a0cc02520.png" />\
          	</div>\
          </div>\
        </div>\
\
        <div id="s-Polygon_1" class="path firer commentable non-processed" customid="X"   datasizewidth="14.0px" datasizeheight="14.0px" dataX="1337.7" dataY="74.5"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="14.0" height="14.0" viewBox="1337.6605758666992 74.50000000000003 14.0 14.0" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Polygon_1-e0ca8" d="M1351.6605758666992 75.90999996662143 L1350.250576019287 74.50000000000003 L1344.6605758666992 80.09000015258792 L1339.0705758333206 74.50000000000003 L1337.6605758666992 75.90999996662143 L1343.250576019287 81.50000000000003 L1337.6605758666992 87.09000015258792 L1339.0705758333206 88.50000000000003 L1344.6605758666992 82.90999984741214 L1350.250576019287 88.50000000000003 L1351.6605758666992 87.09000015258792 L1346.0705757141113 81.50000000000003 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Polygon_1-e0ca8" fill="#000000" fill-opacity="1.0" opacity="0.4"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_14" class="group firer ie-background commentable non-processed" customid="Form" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Paragraph_2" class="richtext manualfit firer ie-background commentable non-processed" customid="Have an account"   datasizewidth="200.7px" datasizeheight="27.0px" dataX="90.2" dataY="820.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_2_0">&iquest;Tienes una cuenta? </span><span id="rtr-s-Paragraph_2_1">Inicia sesi&oacute;n</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Button_2" class="button multiline manualfit firer commentable non-processed" customid="Login"   datasizewidth="149.5px" datasizeheight="47.0px" dataX="501.0" dataY="759.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Button_2_0">Continuar</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_18" class="group firer ie-background commentable non-processed" customid="Confirmar Contrase&ntilde;a" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_10" class="password firer commentable non-processed" customid="Input search"  datasizewidth="273.2px" datasizeheight="55.0px" dataX="389.2" dataY="665.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="password"  value="" maxlength="100"  tabindex="-1" placeholder="Repita la contrase&ntilde;a"/></div></div></div></div></div>\
          <div id="s-Paragraph_13" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="130.8px" datasizeheight="32.0px" dataX="411.2" dataY="672.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_13_0">Confirmar contrase&ntilde;a</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_3" class="path firer commentable non-processed" customid="Eye-icon"   datasizewidth="19.7px" datasizeheight="13.3px" dataX="623.0" dataY="692.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="19.700927734375" height="13.2916259765625" viewBox="623.0 692.0 19.700927734375 13.2916259765625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_3-e0ca8" d="M632.8504514669137 692.0 C628.3729735274076 692.0 624.5492072418141 694.7557961936814 623.0 698.6458108108109 C624.5492072418141 702.5358248645679 628.3729735274076 705.2916216216215 632.8504514669137 705.2916216216215 C637.32792940642 705.2916216216215 641.1516959766834 702.5358248645679 642.7009029338275 698.6458108108109 C641.1516959766834 694.7557961936814 637.32792940642 692.0 632.8504514669137 692.0 Z M632.8504514669137 703.0763513513513 C630.3788834393434 703.0763513513513 628.3729735274076 701.0914688286308 628.3729735274076 698.6458108108109 C628.3729735274076 696.2001525113044 630.3788834393434 694.2152702702701 632.8504514669137 694.2152702702701 C635.3220189251431 694.2152702702701 637.32792940642 696.2001525113044 637.32792940642 698.6458108108109 C637.32792940642 701.0914688286308 635.3220189251431 703.0763513513513 632.8504514669137 703.0763513513513 Z M632.8504514669137 695.9874864864864 C631.3639289276394 695.9874864864864 630.16396470321 697.1748714865607 630.16396470321 698.6458108108109 C630.16396470321 700.1167501350608 631.3639289276394 701.304135135135 632.8504514669137 701.304135135135 C634.3369740061879 701.304135135135 635.5369382306175 700.1167501350608 635.5369382306175 698.6458108108109 C635.5369382306175 697.1748714865607 634.3369740061879 695.9874864864864 632.8504514669137 695.9874864864864 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_3-e0ca8" fill="#666666" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_17" class="group firer ie-background commentable non-processed" customid="Contrase&ntilde;a" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_5" class="password firer commentable non-processed" customid="Input search"  datasizewidth="273.2px" datasizeheight="55.0px" dataX="90.2" dataY="665.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="password"  value="" maxlength="100"  tabindex="-1" placeholder="Debe incluir letras y n&uacute;meros"/></div></div></div></div></div>\
          <div id="s-Paragraph_6" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="130.8px" datasizeheight="16.0px" dataX="112.2" dataY="672.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_6_0">Contrase&ntilde;a</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_2" class="path firer commentable non-processed" customid="Eye-icon"   datasizewidth="19.7px" datasizeheight="13.3px" dataX="324.0" dataY="692.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="19.700927734375" height="13.2916259765625" viewBox="324.0 692.0 19.700927734375 13.2916259765625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_2-e0ca8" d="M333.8504514669137 692.0 C329.3729735274075 692.0 325.5492072418141 694.7557961936814 324.0 698.6458108108109 C325.5492072418141 702.5358248645679 329.3729735274075 705.2916216216215 333.8504514669137 705.2916216216215 C338.32792940642 705.2916216216215 342.1516959766834 702.5358248645679 343.7009029338275 698.6458108108109 C342.1516959766834 694.7557961936814 338.32792940642 692.0 333.8504514669137 692.0 Z M333.8504514669137 703.0763513513513 C331.37888343934344 703.0763513513513 329.3729735274075 701.0914688286308 329.3729735274075 698.6458108108109 C329.3729735274075 696.2001525113044 331.37888343934344 694.2152702702701 333.8504514669137 694.2152702702701 C336.3220189251431 694.2152702702701 338.32792940642 696.2001525113044 338.32792940642 698.6458108108109 C338.32792940642 701.0914688286308 336.3220189251431 703.0763513513513 333.8504514669137 703.0763513513513 Z M333.8504514669137 695.9874864864864 C332.36392892763945 695.9874864864864 331.16396470320996 697.1748714865607 331.16396470320996 698.6458108108109 C331.16396470320996 700.1167501350608 332.36392892763945 701.304135135135 333.8504514669137 701.304135135135 C335.3369740061879 701.304135135135 336.53693823061747 700.1167501350608 336.53693823061747 698.6458108108109 C336.53693823061747 697.1748714865607 335.3369740061879 695.9874864864864 333.8504514669137 695.9874864864864 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_2-e0ca8" fill="#666666" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_13" class="group firer ie-background commentable non-processed" customid="ID# Input" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_9" class="number text firer commentable non-processed" customid="Input search"  datasizewidth="273.2px" datasizeheight="55.0px" dataX="389.2" dataY="584.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="number"  value="" maxlength="100"  tabindex="-1" placeholder="0123456789"/></div></div>  </div></div></div>\
          <div id="s-Paragraph_3" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="130.8px" datasizeheight="16.0px" dataX="411.2" dataY="591.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_3_0">N&uacute;mero de documento</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_11" class="group firer ie-background commentable non-processed" customid="ID Selection" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_7" class="number text firer commentable non-processed" customid="Input search"  datasizewidth="273.2px" datasizeheight="55.0px" dataX="90.4" dataY="583.5" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="number"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
          <div id="s-Paragraph_11" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="130.8px" datasizeheight="16.0px" dataX="112.5" dataY="590.5" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_11_0">Tipo de documento</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Category_1" class="dropdown firer commentable non-processed" customid="Category 1"    datasizewidth="239.0px" datasizeheight="29.0px" dataX="107.0" dataY="603.0"  tabindex="-1"><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content icon"><div class="valign"><div class="value">C&eacute;dula de ciudadan&iacute;a</div></div></div></div></div><select id="s-Category_1-options" class="s-e0ca8585-8466-4ed8-b424-0a73cb2314d5 dropdown-options" ><option selected="selected" class="option">C&eacute;dula de ciudadan&iacute;a</option>\
          <option  class="option">Otro documento</option></select></div>\
        </div>\
\
\
        <div id="s-Group_10" class="group firer ie-background commentable non-processed" customid="DOB Input" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_6" class="text firer commentable non-processed" customid="Input search"  datasizewidth="272.8px" datasizeheight="55.0px" dataX="389.2" dataY="502.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
          <div id="s-Paragraph_10" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="214.0px" datasizeheight="36.0px" dataX="411.3" dataY="508.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_10_0">Fecha de nacimiento </span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Input_8" class="date firer commentable non-processed" customid="Fecha de nacimiento" value="1676419200000" format="MM/dd/yyyy"  datasizewidth="171.1px" datasizeheight="29.0px" dataX="406.4" dataY="521.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="date"  tabindex="-1"  /></div></div></div></div></div>\
        </div>\
\
\
        <div id="s-Group_16" class="group firer ie-background commentable non-processed" customid="Email Input" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_4" class="number text firer commentable non-processed" customid="Input search"  datasizewidth="273.2px" datasizeheight="55.0px" dataX="90.2" dataY="502.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="number"  value="" maxlength="100"  tabindex="-1" placeholder="ejemplo@correo.com"/></div></div>  </div></div></div>\
          <div id="s-Paragraph_4" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="107.8px" datasizeheight="18.0px" dataX="112.2" dataY="509.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_4_0">Correo electr&oacute;nico</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_4" class="group firer ie-background commentable non-processed" customid="Surnames input" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_3" class="text firer commentable non-processed" customid="Input search"  datasizewidth="272.8px" datasizeheight="55.0px" dataX="389.2" dataY="421.7" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="Ingrese sus apellidos"/></div></div>  </div></div></div>\
          <div id="s-Paragraph_9" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="59.7px" datasizeheight="18.0px" dataX="411.6" dataY="428.7" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_9_0">Apellidos</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_2" class="group firer ie-background commentable non-processed" customid="Name input" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_2" class="text firer commentable non-processed" customid="Input search"  datasizewidth="272.8px" datasizeheight="55.0px" dataX="90.5" dataY="421.7" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="Ingrese su nombre(s)"/></div></div>  </div></div></div>\
          <div id="s-Paragraph_5" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="73.7px" datasizeheight="18.0px" dataX="112.6" dataY="428.7" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_5_0">Nombre(s)</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
\
      <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Log In buttons" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Group_15" class="group firer ie-background commentable non-processed" customid="or" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Line_1" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="105.5px" datasizeheight="3.0px" dataX="213.0" dataY="377.1"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="105.5" height="2.0" viewBox="213.0 377.14619883040933 105.5 2.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Line_1-e0ca8" d="M214.0 378.14619883040933 L317.5 378.14619883040933 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Line_1-e0ca8" fill="none" stroke-width="1.0" stroke="#E7E7E7" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Line_2" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="104.5px" datasizeheight="3.0px" dataX="438.0" dataY="377.1"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="104.5" height="2.0" viewBox="438.0 377.14619883040933 104.5 2.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Line_2-e0ca8" d="M439.0 378.14619883040933 L541.5 378.14619883040933 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Line_2-e0ca8" fill="none" stroke-width="1.0" stroke="#E7E7E7" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_12" class="richtext manualfit firer ie-background commentable non-processed" customid="or"   datasizewidth="139.5px" datasizeheight="19.0px" dataX="308.0" dataY="373.3" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_12_0">Crea una cuenta</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Paragraph_1" class="richtext manualfit firer ie-background commentable non-processed" customid="Login your account"   datasizewidth="296.5px" datasizeheight="62.0px" dataX="234.0" dataY="300.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_1_0">Bienvenido a Banconfiar</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_3" class="group firer ie-background commentable non-processed" customid="T&eacute;rminos" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Paragraph_7" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="360.9px" datasizeheight="38.0px" dataX="119.0" dataY="762.3" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_7_0">Al crear una cuenta, acepto los t&eacute;rminos de servicio y privacidad</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Input_1" class="checkbox firer commentable non-processed unchecked" customid="Input 1"  datasizewidth="15.0px" datasizeheight="15.0px" dataX="96.8" dataY="764.3"      tabindex="-1">\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Button_1" class="button multiline manualfit firer click ie-background commentable non-processed" customid="Button"   datasizewidth="71.0px" datasizeheight="19.0px" dataX="217.7" dataY="820.5" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Button_1_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;