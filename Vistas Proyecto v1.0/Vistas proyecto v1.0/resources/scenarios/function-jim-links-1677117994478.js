(function(window, undefined) {

  var jimLinks = {
    "57483f01-aea4-495e-b69e-73bd00aeaa78" : {
      "Button_1" : [
        "ad239ebe-6a40-468c-a3a7-488eb106bd3e"
      ],
      "Rectangle_5" : [
        "fc5d9b71-893d-4d5a-9ab9-2314fe70c51f"
      ]
    },
    "fc5d9b71-893d-4d5a-9ab9-2314fe70c51f" : {
      "Button_1" : [
        "ad239ebe-6a40-468c-a3a7-488eb106bd3e"
      ],
      "Button_3" : [
        "57483f01-aea4-495e-b69e-73bd00aeaa78"
      ],
      "Button_4" : [
        "ad239ebe-6a40-468c-a3a7-488eb106bd3e"
      ],
      "Button_2" : [
        "fc5d9b71-893d-4d5a-9ab9-2314fe70c51f"
      ]
    },
    "ad239ebe-6a40-468c-a3a7-488eb106bd3e" : {
      "Rectangle_4" : [
        "fc5d9b71-893d-4d5a-9ab9-2314fe70c51f"
      ]
    },
    "e0ca8585-8466-4ed8-b424-0a73cb2314d5" : {
      "Button_1" : [
        "a1a78579-69b7-404d-b367-ae4ffb8d1db8"
      ]
    },
    "a1a78579-69b7-404d-b367-ae4ffb8d1db8" : {
      "Button_1" : [
        "fc5d9b71-893d-4d5a-9ab9-2314fe70c51f"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);