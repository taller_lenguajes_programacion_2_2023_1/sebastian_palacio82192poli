-- Generado por Oracle SQL Developer Data Modeler 22.2.0.165.1149
--   en:        2023-02-22 20:11:28 COT
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



-- predefined type, no DDL - MDSYS.SDO_GEOMETRY

-- predefined type, no DDL - XMLTYPE

CREATE TABLE cliente (
    nombre            VARCHAR2(50) NOT NULL,
    apellidos         VARCHAR2(50) NOT NULL,
    correo            VARCHAR2(50) NOT NULL,
    passwd            VARCHAR2(50),
    tipo_documento    VARCHAR2(30) NOT NULL,
    numdoc            VARCHAR2(15) NOT NULL,
    dob               DATE NOT NULL,
    sesi�n_num_sesion INTEGER NOT NULL
);

ALTER TABLE cliente ADD CONSTRAINT cliente_pk PRIMARY KEY ( numdoc );

CREATE TABLE cr�dito (
    id_credito     INTEGER NOT NULL,
    cantidad       INTEGER NOT NULL,
    aprobado       CHAR(1) NOT NULL,
    cliente_numdoc VARCHAR2(15) NOT NULL
);

ALTER TABLE cr�dito ADD CONSTRAINT cr�dito_pk PRIMARY KEY ( id_credito );

CREATE TABLE cuenta_bancaria (
    num_cuenta     INTEGER NOT NULL,
    saldo          INTEGER NOT NULL,
    estado         VARCHAR2(15) NOT NULL,
    movimientos    INTEGER NOT NULL,
    cliente_numdoc VARCHAR2(15) NOT NULL
);

ALTER TABLE cuenta_bancaria ADD CONSTRAINT cuenta_pk PRIMARY KEY ( num_cuenta );

CREATE TABLE deuda (
    id_deuda            INTEGER NOT NULL,
    balance             INTEGER NOT NULL,
    cuota_mensual       INTEGER NOT NULL,
    termino             INTEGER NOT NULL,
    atraso              CHAR(1) NOT NULL,
    cliente_numdoc      VARCHAR2(15) NOT NULL,
    metodo_pago_id_pago INTEGER NOT NULL
);

ALTER TABLE deuda ADD CONSTRAINT deuda_pk PRIMARY KEY ( id_deuda );

CREATE TABLE metodo_pago (
    tipo_metodo    VARCHAR2(30) NOT NULL,
    id_pago        INTEGER NOT NULL,
    num_metod      INTEGER NOT NULL,
    deuda_id_deuda INTEGER NOT NULL,
    cliente_numdoc VARCHAR2(15) NOT NULL
);

ALTER TABLE metodo_pago ADD CONSTRAINT metodo_pago_pk PRIMARY KEY ( id_pago );

CREATE TABLE sesi�n (
    num_sesion      INTEGER NOT NULL,
    "tiempo sesion" INTEGER NOT NULL
);

ALTER TABLE sesi�n ADD CONSTRAINT sesi�n_pk PRIMARY KEY ( num_sesion );

ALTER TABLE cliente
    ADD CONSTRAINT cliente_sesi�n_fk FOREIGN KEY ( sesi�n_num_sesion )
        REFERENCES sesi�n ( num_sesion );

ALTER TABLE cr�dito
    ADD CONSTRAINT cr�dito_cliente_fk FOREIGN KEY ( cliente_numdoc )
        REFERENCES cliente ( numdoc );

ALTER TABLE cuenta_bancaria
    ADD CONSTRAINT cuenta_bancaria_cliente_fk FOREIGN KEY ( cliente_numdoc )
        REFERENCES cliente ( numdoc );

ALTER TABLE deuda
    ADD CONSTRAINT deuda_cliente_fk FOREIGN KEY ( cliente_numdoc )
        REFERENCES cliente ( numdoc );

ALTER TABLE deuda
    ADD CONSTRAINT deuda_metodo_pago_fk FOREIGN KEY ( metodo_pago_id_pago )
        REFERENCES metodo_pago ( id_pago );

ALTER TABLE metodo_pago
    ADD CONSTRAINT metodo_pago_cliente_fk FOREIGN KEY ( cliente_numdoc )
        REFERENCES cliente ( numdoc );

ALTER TABLE metodo_pago
    ADD CONSTRAINT metodo_pago_deuda_fk FOREIGN KEY ( deuda_id_deuda )
        REFERENCES deuda ( id_deuda )
            ON DELETE CASCADE;



-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             6
-- CREATE INDEX                             0
-- ALTER TABLE                             13
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE MATERIALIZED VIEW LOG             0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
