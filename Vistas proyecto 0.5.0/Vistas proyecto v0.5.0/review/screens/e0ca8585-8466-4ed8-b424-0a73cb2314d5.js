var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1440" deviceHeight="900">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1024" height="768">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677031595099.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-e0ca8585-8466-4ed8-b424-0a73cb2314d5" class="screen growth-vertical devWeb canvas PORTRAIT firer commentable non-processed" alignment="left" name="Website login" width="1440" height="900">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/e0ca8585-8466-4ed8-b424-0a73cb2314d5-1677031595099.css" />\
      <div class="freeLayout">\
      <div id="s-Group_5" class="group firer ie-background commentable non-processed" customid="Base popup" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_3" class="rectangle manualfit firer commentable non-processed" customid="Base"   datasizewidth="1287.0px" datasizeheight="789.0px" datasizewidthpx="1287.0" datasizeheightpx="788.9999999999999" dataX="84.0" dataY="46.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_3_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_1" class="rectangle manualfit firer commentable non-processed" customid="Picture"   datasizewidth="652.0px" datasizeheight="789.0px" datasizewidthpx="652.0" datasizeheightpx="789.0000000000002" dataX="715.0" dataY="46.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_1_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Polygon_1" class="path firer commentable non-processed" customid="X"   datasizewidth="14.0px" datasizeheight="14.0px" dataX="1338.0" dataY="64.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="14.0" height="14.0" viewBox="1338.0 64.0 14.0 14.0" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Polygon_1-e0ca8" d="M1352.0 65.4099999666214 L1350.590000152588 64.0 L1345.0 69.59000015258789 L1339.4099999666214 64.0 L1338.0 65.4099999666214 L1343.590000152588 71.0 L1338.0 76.59000015258789 L1339.4099999666214 78.0 L1345.0 72.40999984741211 L1350.590000152588 78.0 L1352.0 76.59000015258789 L1346.409999847412 71.0 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Polygon_1-e0ca8" fill="#000000" fill-opacity="1.0" opacity="0.4"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_12" class="group firer ie-background commentable non-processed" customid="Logo" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Path_1" class="path firer commentable non-processed" customid="symbol"   datasizewidth="24.8px" datasizeheight="28.6px" dataX="322.0" dataY="230.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="24.799999237060547" height="28.644962310791016" viewBox="322.0 230.0 24.799999237060547 28.644962310791016" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_1-e0ca8" d="M334.39999981347694 230.0 L322.0 237.1661288270724 L322.0 251.48860944722742 L334.39999981347694 258.64496124031007 L346.8 251.48860944722742 L346.8 237.1661288270724 L334.39999981347694 230.0 Z M337.4119874664332 244.32248062015503 L343.7880123470438 240.63676368973162 L343.7880123470438 247.99842126247168 L337.4119874664332 244.32248062015503 Z M342.2820192666578 238.03623114858704 L334.39999981347694 242.58227448535982 L326.5179811063883 238.03623114858704 L334.39999981347694 233.4804115237075 L342.2820192666578 238.03623114858704 Z M331.3880121605207 244.32248062015503 L325.01198727991016 248.00819717763696 L325.01198727991016 240.64653960489693 L331.3880121605207 244.32248062015503 Z M326.5179811063883 250.60873009172303 L334.39999981347694 246.05291046684349 L342.28201852056566 250.60873009172303 L334.39999981347694 255.1645489707196 L326.5179811063883 250.60873009172303 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_1-e0ca8" fill="#1877F2" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_8" class="richtext manualfit firer ie-background commentable non-processed" customid="Login your account"   datasizewidth="122.5px" datasizeheight="25.0px" dataX="358.0" dataY="233.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_8_0">InsideBox</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_14" class="group firer ie-background commentable non-processed" customid="Form" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Paragraph_2" class="richtext manualfit firer ie-background commentable non-processed" customid="Have an account"   datasizewidth="200.7px" datasizeheight="27.0px" dataX="114.2" dataY="809.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_2_0">&iquest;Tienes una cuenta? </span><span id="rtr-s-Paragraph_2_1">Inicia sesi&oacute;n</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Button_2" class="button multiline manualfit firer commentable non-processed" customid="Login"   datasizewidth="149.5px" datasizeheight="47.0px" dataX="526.4" dataY="756.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Button_2_0">Continuar</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_18" class="group firer ie-background commentable non-processed" customid="Confirmar Contrase&ntilde;a" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_10" class="password firer commentable non-processed" customid="Input search"  datasizewidth="273.2px" datasizeheight="55.0px" dataX="413.2" dataY="654.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="password"  value="" maxlength="100"  tabindex="-1" placeholder="Repita la contrase&ntilde;a"/></div></div></div></div></div>\
          <div id="s-Paragraph_13" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="130.8px" datasizeheight="32.0px" dataX="435.2" dataY="661.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_13_0">Confirmar contrase&ntilde;a</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_3" class="path firer commentable non-processed" customid="Eye-icon"   datasizewidth="19.7px" datasizeheight="13.3px" dataX="647.0" dataY="681.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="19.700927734375" height="13.2916259765625" viewBox="647.0 681.0 19.700927734375 13.2916259765625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_3-e0ca8" d="M656.8504514669137 681.0 C652.3729735274076 681.0 648.5492072418141 683.7557961936814 647.0 687.6458108108109 C648.5492072418141 691.5358248645679 652.3729735274076 694.2916216216215 656.8504514669137 694.2916216216215 C661.32792940642 694.2916216216215 665.1516959766834 691.5358248645679 666.7009029338275 687.6458108108109 C665.1516959766834 683.7557961936814 661.32792940642 681.0 656.8504514669137 681.0 Z M656.8504514669137 692.0763513513513 C654.3788834393434 692.0763513513513 652.3729735274076 690.0914688286308 652.3729735274076 687.6458108108109 C652.3729735274076 685.2001525113044 654.3788834393434 683.2152702702701 656.8504514669137 683.2152702702701 C659.3220189251431 683.2152702702701 661.32792940642 685.2001525113044 661.32792940642 687.6458108108109 C661.32792940642 690.0914688286308 659.3220189251431 692.0763513513513 656.8504514669137 692.0763513513513 Z M656.8504514669137 684.9874864864864 C655.3639289276394 684.9874864864864 654.16396470321 686.1748714865607 654.16396470321 687.6458108108109 C654.16396470321 689.1167501350608 655.3639289276394 690.304135135135 656.8504514669137 690.304135135135 C658.3369740061879 690.304135135135 659.5369382306175 689.1167501350608 659.5369382306175 687.6458108108109 C659.5369382306175 686.1748714865607 658.3369740061879 684.9874864864864 656.8504514669137 684.9874864864864 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_3-e0ca8" fill="#666666" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_17" class="group firer ie-background commentable non-processed" customid="Contrase&ntilde;a" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_5" class="password firer commentable non-processed" customid="Input search"  datasizewidth="273.2px" datasizeheight="55.0px" dataX="114.2" dataY="654.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="password"  value="" maxlength="100"  tabindex="-1" placeholder="Debe incluir letras y n&uacute;meros"/></div></div></div></div></div>\
          <div id="s-Paragraph_6" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="130.8px" datasizeheight="16.0px" dataX="136.2" dataY="661.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_6_0">Contrase&ntilde;a</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_2" class="path firer commentable non-processed" customid="Eye-icon"   datasizewidth="19.7px" datasizeheight="13.3px" dataX="348.0" dataY="681.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="19.700927734375" height="13.2916259765625" viewBox="348.0 681.0 19.700927734375 13.2916259765625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_2-e0ca8" d="M357.8504514669137 681.0 C353.3729735274075 681.0 349.5492072418141 683.7557961936814 348.0 687.6458108108109 C349.5492072418141 691.5358248645679 353.3729735274075 694.2916216216215 357.8504514669137 694.2916216216215 C362.32792940642 694.2916216216215 366.1516959766834 691.5358248645679 367.7009029338275 687.6458108108109 C366.1516959766834 683.7557961936814 362.32792940642 681.0 357.8504514669137 681.0 Z M357.8504514669137 692.0763513513513 C355.37888343934344 692.0763513513513 353.3729735274075 690.0914688286308 353.3729735274075 687.6458108108109 C353.3729735274075 685.2001525113044 355.37888343934344 683.2152702702701 357.8504514669137 683.2152702702701 C360.3220189251431 683.2152702702701 362.32792940642 685.2001525113044 362.32792940642 687.6458108108109 C362.32792940642 690.0914688286308 360.3220189251431 692.0763513513513 357.8504514669137 692.0763513513513 Z M357.8504514669137 684.9874864864864 C356.36392892763945 684.9874864864864 355.16396470320996 686.1748714865607 355.16396470320996 687.6458108108109 C355.16396470320996 689.1167501350608 356.36392892763945 690.304135135135 357.8504514669137 690.304135135135 C359.3369740061879 690.304135135135 360.53693823061747 689.1167501350608 360.53693823061747 687.6458108108109 C360.53693823061747 686.1748714865607 359.3369740061879 684.9874864864864 357.8504514669137 684.9874864864864 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_2-e0ca8" fill="#666666" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_13" class="group firer ie-background commentable non-processed" customid="ID# Input" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_9" class="number text firer commentable non-processed" customid="Input search"  datasizewidth="273.2px" datasizeheight="55.0px" dataX="413.2" dataY="573.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="number"  value="" maxlength="100"  tabindex="-1" placeholder="0123456789"/></div></div>  </div></div></div>\
          <div id="s-Paragraph_3" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="130.8px" datasizeheight="16.0px" dataX="435.2" dataY="580.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_3_0">N&uacute;mero de documento</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_11" class="group firer ie-background commentable non-processed" customid="ID Selection" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_7" class="number text firer commentable non-processed" customid="Input search"  datasizewidth="273.2px" datasizeheight="55.0px" dataX="114.4" dataY="572.5" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="number"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
          <div id="s-Paragraph_11" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="130.8px" datasizeheight="16.0px" dataX="136.5" dataY="579.5" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_11_0">Tipo de documento</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Category_1" class="dropdown firer commentable non-processed" customid="Category 1"    datasizewidth="239.0px" datasizeheight="29.0px" dataX="131.0" dataY="592.0"  tabindex="-1"><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content icon"><div class="valign"><div class="value">C&eacute;dula de ciudadan&iacute;a</div></div></div></div></div><select id="s-Category_1-options" class="s-e0ca8585-8466-4ed8-b424-0a73cb2314d5 dropdown-options" ><option selected="selected" class="option">C&eacute;dula de ciudadan&iacute;a</option>\
          <option  class="option">Otro documento</option></select></div>\
        </div>\
\
\
        <div id="s-Group_10" class="group firer ie-background commentable non-processed" customid="DOB Input" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_6" class="text firer commentable non-processed" customid="Input search"  datasizewidth="272.8px" datasizeheight="55.0px" dataX="413.2" dataY="491.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
          <div id="s-Paragraph_10" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="214.0px" datasizeheight="36.0px" dataX="435.3" dataY="497.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_10_0">Fecha de nacimiento </span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Input_8" class="date firer commentable non-processed" customid="Fecha de nacimiento" value="1676419200000" format="MM/dd/yyyy"  datasizewidth="171.1px" datasizeheight="29.0px" dataX="430.4" dataY="510.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="date"  tabindex="-1"  /></div></div></div></div></div>\
        </div>\
\
\
        <div id="s-Group_16" class="group firer ie-background commentable non-processed" customid="Email Input" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_4" class="number text firer commentable non-processed" customid="Input search"  datasizewidth="273.2px" datasizeheight="55.0px" dataX="114.2" dataY="491.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="number"  value="" maxlength="100"  tabindex="-1" placeholder="ejemplo@correo.com"/></div></div>  </div></div></div>\
          <div id="s-Paragraph_4" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="107.8px" datasizeheight="18.0px" dataX="136.2" dataY="498.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_4_0">Correo electr&oacute;nico</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_4" class="group firer ie-background commentable non-processed" customid="Surnames input" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_3" class="text firer commentable non-processed" customid="Input search"  datasizewidth="272.8px" datasizeheight="55.0px" dataX="413.2" dataY="410.7" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="Ingrese sus apellidos"/></div></div>  </div></div></div>\
          <div id="s-Paragraph_9" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="59.7px" datasizeheight="18.0px" dataX="435.6" dataY="417.7" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_9_0">Apellidos</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_2" class="group firer ie-background commentable non-processed" customid="Name input" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_2" class="text firer commentable non-processed" customid="Input search"  datasizewidth="272.8px" datasizeheight="55.0px" dataX="114.5" dataY="410.7" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="Ingrese su nombre(s)"/></div></div>  </div></div></div>\
          <div id="s-Paragraph_5" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="73.7px" datasizeheight="18.0px" dataX="136.6" dataY="417.7" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_5_0">Nombre(s)</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
\
      <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Log In buttons" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Group_15" class="group firer ie-background commentable non-processed" customid="or" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Line_1" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="105.5px" datasizeheight="3.0px" dataX="237.0" dataY="366.1"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="105.5" height="2.0" viewBox="237.0 366.14619883040933 105.5 2.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Line_1-e0ca8" d="M238.0 367.14619883040933 L341.5 367.14619883040933 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Line_1-e0ca8" fill="none" stroke-width="1.0" stroke="#E7E7E7" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Line_2" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="104.5px" datasizeheight="3.0px" dataX="462.0" dataY="366.1"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="104.5" height="2.0" viewBox="462.0 366.14619883040933 104.5 2.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Line_2-e0ca8" d="M463.0 367.14619883040933 L565.5 367.14619883040933 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Line_2-e0ca8" fill="none" stroke-width="1.0" stroke="#E7E7E7" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_12" class="richtext manualfit firer ie-background commentable non-processed" customid="or"   datasizewidth="139.5px" datasizeheight="19.0px" dataX="332.0" dataY="362.3" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_12_0">Crea una cuenta</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Paragraph_1" class="richtext manualfit firer ie-background commentable non-processed" customid="Login your account"   datasizewidth="296.5px" datasizeheight="62.0px" dataX="258.0" dataY="289.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_1_0">Bienvenido al nuevo proyecto</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_3" class="group firer ie-background commentable non-processed" customid="T&eacute;rminos" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Paragraph_7" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="360.9px" datasizeheight="38.0px" dataX="143.0" dataY="762.3" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_7_0">Al crear una cuenta, acepto los t&eacute;rminos de servicio y privacidad</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Input_1" class="checkbox firer commentable non-processed unchecked" customid="Input 1"  datasizewidth="15.0px" datasizeheight="15.0px" dataX="120.8" dataY="764.3"      tabindex="-1">\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
        </div>\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;