var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1920" deviceHeight="1080">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1024" height="768">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677031595099.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-57483f01-aea4-495e-b69e-73bd00aeaa78" class="screen growth-vertical devWeb canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="Transferencia" width="1920" height="1080">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/57483f01-aea4-495e-b69e-73bd00aeaa78-1677031595099.css" />\
      <div class="freeLayout">\
      <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Techo" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_1" class="rectangle manualfit firer commentable non-processed" customid="Base"   datasizewidth="1920.0px" datasizeheight="73.7px" datasizewidthpx="1920.0" datasizeheightpx="73.65738761775151" dataX="0.0" dataY="-0.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_1_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Path_1" class="path firer commentable non-processed" customid="tmpSVG"   datasizewidth="46.0px" datasizeheight="46.0px" dataX="1856.0" dataY="13.8"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="46.0" height="46.0" viewBox="1856.0 13.828693808875585 46.0 46.0" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_1-57483" d="M1879.0 23.891193808875585 C1874.6335938870907 23.891193808875585 1871.09375 27.431037695966268 1871.09375 31.797443808875585 C1871.09375 36.16385060723832 1874.6335932016373 39.703693808875585 1879.0 39.703693808875585 C1883.3664067983627 39.703693808875585 1886.90625 36.1638499217849 1886.90625 31.797443808875585 C1886.90625 27.431037010512853 1883.3664067983627 23.891193808875585 1879.0 23.891193808875585 Z M1879.0 35.391193808875585 C1877.0180469229817 35.391193808875585 1875.40625 33.77849842783024 1875.40625 31.797443808875585 C1875.40625 29.811896385512853 1877.0144525766373 28.203693808875585 1879.0 28.203693808875585 C1880.9855474233627 28.203693808875585 1882.59375 29.815490731857324 1882.59375 31.797443808875585 C1882.59375 33.78299123223832 1880.9855474233627 35.391193808875585 1879.0 35.391193808875585 Z M1879.0 13.828693808875585 C1866.2960936129093 13.828693808875585 1856.0 24.1247874217849 1856.0 36.828693808875585 C1856.0 49.53260019596627 1866.2960936129093 59.828693808875585 1879.0 59.828693808875585 C1891.7039063870907 59.828693808875585 1902.0 49.53260019596627 1902.0 36.828693808875585 C1902.0 24.1247874217849 1891.7039057016373 13.828693808875585 1879.0 13.828693808875585 Z M1879.0 55.516193808875585 C1874.8016016036272 55.516193808875585 1870.935624808073 54.10744378145745 1867.814453125 51.761623414121175 C1869.3687502741814 48.777912558875585 1872.3874994516373 46.891193808875585 1875.7835934758186 46.891193808875585 L1882.2244921326637 46.891193808875585 C1885.6160936951637 46.891193808875585 1888.6312497258186 48.77881101693919 1890.1918359100819 51.761623414121175 C1887.0679676532745 54.10564583715012 1883.1957042217255 55.516193808875585 1879.0 55.516193808875585 Z M1893.3929698467255 48.732990683875585 C1890.9671885967255 44.94158333715012 1886.8343760967255 42.578693808875585 1882.2164051532745 42.578693808875585 L1875.7835934758186 42.578693808875585 C1871.1692184209824 42.578693808875585 1867.0373045504093 44.937092246375585 1864.607030838728 48.73119411047509 C1861.9260936677456 45.498615683875585 1860.3125 41.34783333715012 1860.3125 36.828693808875585 C1860.3125 26.52361595805695 1868.695820093155 18.141193808875585 1879.0 18.141193808875585 C1889.304179906845 18.141193808875585 1897.6875 26.524513902030492 1897.6875 36.828693808875585 C1897.6875 41.34783333715012 1896.0703125 45.498615683875585 1893.3929698467255 48.732990683875585 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_1-57483" fill="#434343" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_1" class="richtext manualfit firer ie-background commentable non-processed" customid="Sebasti&aacute;n"   datasizewidth="153.0px" datasizeheight="27.0px" dataX="1702.0" dataY="23.3" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_1_0">Sebasti&aacute;n</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Image_1" class="image lockH firer ie-background commentable non-processed" customid="an-tpr-eid-sacrifice-01"   datasizewidth="81.1px" datasizeheight="54.6px" dataX="25.0" dataY="9.5" aspectRatio="1.484375"   alt="image">\
          <div class="borderLayer">\
          	<div class="imageViewport">\
          		<img src="./images/0bb8d8c4-618a-4851-935e-1e6b98d47949.jpg" />\
          	</div>\
          </div>\
        </div>\
\
        <div id="s-Button_1" class="button multiline manualfit firer mouseenter mouseleave ie-background commentable non-processed" customid="Button"   datasizewidth="79.0px" datasizeheight="74.0px" dataX="1841.0" dataY="-0.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Button_1_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;