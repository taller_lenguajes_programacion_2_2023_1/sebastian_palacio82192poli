var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1920" deviceHeight="1080">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1024" height="768">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677031595099.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-a1a78579-69b7-404d-b367-ae4ffb8d1db8" class="screen growth-vertical devWeb canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="Inicio de sesi&oacute;n" width="1920" height="1080">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/a1a78579-69b7-404d-b367-ae4ffb8d1db8-1677031595099.css" />\
      <div class="freeLayout">\
      <div id="s-Rectangle_2" class="rectangle manualfit firer commentable non-processed" customid="Base"   datasizewidth="540.0px" datasizeheight="789.0px" datasizewidthpx="539.9999999999999" datasizeheightpx="788.9999999999999" dataX="690.0" dataY="145.5" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Rectangle_2_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Group_3" class="group firer ie-background commentable non-processed" customid="Email Input" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Input_1" class="number text firer commentable non-processed" customid="Input search"  datasizewidth="328.2px" datasizeheight="55.0px" dataX="795.2" dataY="460.0" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="number"  value="" maxlength="100"  tabindex="-1" placeholder="sucorreo@correo.com"/></div></div>  </div></div></div>\
        <div id="s-Paragraph_3" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="107.8px" datasizeheight="18.0px" dataX="905.1" dataY="465.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_3_0">Correo electr&oacute;nico</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Bienvenida" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Group_2" class="group firer ie-background commentable non-processed" customid="Raya" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Path_1" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="120.4px" datasizeheight="3.0px" dataX="794.0" dataY="429.1"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="120.41531372070312" height="2.0" viewBox="794.0 429.14619883040933 120.41531372070312 2.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_1-a1a78" d="M795.0 430.14619883040933 L913.4153041909171 430.14619883040933 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_1-a1a78" fill="none" stroke-width="1.0" stroke="#E7E7E7" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_2" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="120.6px" datasizeheight="3.0px" dataX="1002.9" dataY="429.1"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="120.55349731445312" height="2.0" viewBox="1002.9465026855469 429.14619883040933 120.55349731445312 2.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_2-a1a78" d="M1003.9465153537653 430.1461988304094 L1122.5 430.14619883040933 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_2-a1a78" fill="none" stroke-width="1.0" stroke="#E7E7E7" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_1" class="richtext manualfit firer ie-background commentable non-processed" customid="Raya"   datasizewidth="139.5px" datasizeheight="19.0px" dataX="889.0" dataY="421.3" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_1_0">Inicia sesi&oacute;n</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Paragraph_2" class="richtext manualfit firer ie-background commentable non-processed" customid="Login your account"   datasizewidth="296.5px" datasizeheight="62.0px" dataX="815.0" dataY="297.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_2_0">&iexcl;Bienvenido de nuevo al proyecto!</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_4" class="group firer ie-background commentable non-processed" customid="Contrase&ntilde;a" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Input_2" class="password firer commentable non-processed" customid="Input search"  datasizewidth="328.2px" datasizeheight="55.0px" dataX="795.2" dataY="560.0" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="password"  value="" maxlength="100"  tabindex="-1" placeholder="Digite su contrase&ntilde;a"/></div></div></div></div></div>\
        <div id="s-Paragraph_4" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="69.9px" datasizeheight="16.0px" dataX="924.1" dataY="565.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_4_0">Contrase&ntilde;a</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Path_10" class="path firer commentable non-processed" customid="Eye-icon"   datasizewidth="19.7px" datasizeheight="13.3px" dataX="1089.0" dataY="587.5"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="19.700927734375" height="13.2916259765625" viewBox="1089.0 587.4999999999998 19.700927734375 13.2916259765625" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_10-a1a78" d="M1098.8504514669137 587.4999999999999 C1094.3729735274076 587.4999999999999 1090.5492072418142 590.2557961936811 1089.0 594.1458108108106 C1090.5492072418142 598.0358248645678 1094.3729735274076 600.7916216216214 1098.8504514669137 600.7916216216214 C1103.32792940642 600.7916216216214 1107.1516959766834 598.0358248645678 1108.7009029338274 594.1458108108106 C1107.1516959766834 590.2557961936811 1103.32792940642 587.4999999999999 1098.8504514669137 587.4999999999999 Z M1098.8504514669137 598.5763513513511 C1096.3788834393436 598.5763513513511 1094.3729735274076 596.5914688286307 1094.3729735274076 594.1458108108106 C1094.3729735274076 591.7001525113044 1096.3788834393436 589.7152702702701 1098.8504514669137 589.7152702702701 C1101.322018925143 589.7152702702701 1103.32792940642 591.7001525113044 1103.32792940642 594.1458108108106 C1103.32792940642 596.5914688286307 1101.322018925143 598.5763513513511 1098.8504514669137 598.5763513513511 Z M1098.8504514669137 591.4874864864862 C1097.3639289276393 591.4874864864862 1096.16396470321 592.6748714865605 1096.16396470321 594.1458108108106 C1096.16396470321 595.6167501350607 1097.3639289276393 596.8041351351349 1098.8504514669137 596.8041351351349 C1100.336974006188 596.8041351351349 1101.5369382306176 595.6167501350607 1101.5369382306176 594.1458108108106 C1101.5369382306176 592.6748714865605 1100.336974006188 591.4874864864862 1098.8504514669137 591.4874864864862 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_10-a1a78" fill="#666666" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Button_1" class="button multiline manualfit firer commentable non-processed" customid="Iniciar sesi&oacute;n"   datasizewidth="327.6px" datasizeheight="49.0px" dataX="795.7" dataY="660.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Button_1_0">Iniciar sesi&oacute;n</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Path_7" class="path firer commentable non-processed" customid="tmpSVG"   datasizewidth="46.0px" datasizeheight="46.0px" dataX="936.7" dataY="368.6"  >\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg xmlns="http://www.w3.org/2000/svg" width="46.0" height="46.0" viewBox="936.7430114746102 368.64619445800804 46.0 46.0" preserveAspectRatio="none">\
          	  <g>\
          	    <defs>\
          	      <path id="s-Path_7-a1a78" d="M959.7430114746102 378.70869445800804 C955.3766053617009 378.70869445800804 951.8367614746102 382.2485383450987 951.8367614746102 386.61494445800804 C951.8367614746102 390.9813512563708 955.3766046762474 394.52119445800804 959.7430114746102 394.52119445800804 C964.1094182729729 394.52119445800804 967.6492614746102 390.98135057091736 967.6492614746102 386.61494445800804 C967.6492614746102 382.2485376596453 964.1094182729729 378.70869445800804 959.7430114746102 378.70869445800804 Z M959.7430114746102 390.20869445800804 C957.7610583975919 390.20869445800804 956.1492614746102 388.5959990769627 956.1492614746102 386.61494445800804 C956.1492614746102 384.6293970346453 957.7574640512474 383.02119445800804 959.7430114746102 383.02119445800804 C961.7285588979729 383.02119445800804 963.3367614746102 384.6329913809898 963.3367614746102 386.61494445800804 C963.3367614746102 388.6004918813708 961.7285588979729 390.20869445800804 959.7430114746102 390.20869445800804 Z M959.7430114746102 368.64619445800804 C947.0391050875195 368.64619445800804 936.7430114746102 378.94228807091736 936.7430114746102 391.64619445800804 C936.7430114746102 404.3501008450987 947.0391050875195 414.64619445800804 959.7430114746102 414.64619445800804 C972.4469178617009 414.64619445800804 982.7430114746102 404.3501008450987 982.7430114746102 391.64619445800804 C982.7430114746102 378.94228807091736 972.4469171762474 368.64619445800804 959.7430114746102 368.64619445800804 Z M959.7430114746102 410.33369445800804 C955.5446130782374 410.33369445800804 951.6786362826832 408.9249444305899 948.5574645996102 406.57912406325363 C950.1117617487915 403.59541320800804 953.1305109262474 401.70869445800804 956.5266049504288 401.70869445800804 L962.9675036072739 401.70869445800804 C966.3591051697739 401.70869445800804 969.3742612004288 403.59631166607164 970.934847384692 406.57912406325363 C967.8109791278847 408.9231464862826 963.9387156963356 410.33369445800804 959.7430114746102 410.33369445800804 Z M974.1359813213356 403.55049133300804 C971.7102000713356 399.7590839862826 967.5773875713356 397.39619445800804 962.9594166278847 397.39619445800804 L956.5266049504288 397.39619445800804 C951.9122298955925 397.39619445800804 947.7803160250195 399.75459289550804 945.3500423133381 403.54869475960754 C942.6691051423558 400.31611633300804 941.0555114746102 396.1653339862826 941.0555114746102 391.64619445800804 C941.0555114746102 381.3411166071894 949.4388315677651 372.95869445800804 959.7430114746102 372.95869445800804 C970.0471913814553 372.95869445800804 978.4305114746102 381.34201455116295 978.4305114746102 391.64619445800804 C978.4305114746102 396.1653339862826 976.8133239746102 400.31611633300804 974.1359813213356 403.55049133300804 Z "></path>\
          	    </defs>\
          	    <g style="mix-blend-mode:normal">\
          	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_7-a1a78" fill="#666666" fill-opacity="1.0"></use>\
          	    </g>\
          	  </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_5" class="richtext manualfit firer ie-background commentable non-processed" customid="Have an account"   datasizewidth="253.2px" datasizeheight="27.0px" dataX="832.9" dataY="716.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_5_0">&iquest;Problema iniciando sesi&oacute;n? </span><span id="rtr-s-Paragraph_5_1">Cont&aacute;ctanos</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Rectangle_1" class="rectangle manualfit firer commentable non-processed" customid="Base"   datasizewidth="1920.0px" datasizeheight="73.7px" datasizewidthpx="1920.0" datasizeheightpx="73.65738761775151" dataX="0.0" dataY="-0.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Rectangle_1_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Image_8" class="image lockH firer ie-background commentable non-processed" customid="an-tpr-eid-sacrifice-01"   datasizewidth="81.1px" datasizeheight="54.6px" dataX="25.0" dataY="9.5" aspectRatio="1.484375"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/0bb8d8c4-618a-4851-935e-1e6b98d47949.jpg" />\
        	</div>\
        </div>\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;