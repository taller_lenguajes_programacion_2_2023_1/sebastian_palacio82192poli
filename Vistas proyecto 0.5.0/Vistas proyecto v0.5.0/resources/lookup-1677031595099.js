(function(window, undefined) {
  var dictionary = {
    "57483f01-aea4-495e-b69e-73bd00aeaa78": "Transferencia",
    "fc5d9b71-893d-4d5a-9ab9-2314fe70c51f": "Perfil",
    "ad239ebe-6a40-468c-a3a7-488eb106bd3e": "Configuración Cuenta",
    "e0ca8585-8466-4ed8-b424-0a73cb2314d5": "Website login",
    "a1a78579-69b7-404d-b367-ae4ffb8d1db8": "Inicio de sesión",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);